/**
 * Initialize Data Table
 * Zero Configuration
 */
/*$(document).ready(function() {
    $('#example').DataTable();
});*/


/**
 * Disable Options
 */
/* $(document).ready(function() {
 $('#example').DataTable( {
 "paging":   false,
 "ordering": false,
 "info":     false
 } );
 } );*/


/**
 * The order parameter is an array of arrays where
 * the first value of the inner array is the column to order on,
 * and the second is 'asc' (ascending ordering) or 'desc' (descending ordering) as required.
 * order is a 2D array to allow multi-column ordering to be defined.
 * The table is ordered (descending) by the Age column.
 */
/*
$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );
*/


/** Multiple Column Ordering */
/*$(document).ready(function() {
 $('#example').DataTable( {
 columnDefs: [ {
 targets: [ 0 ],
 orderData: [ 0, 1 ]
 }, {
 targets: [ 1 ],
 orderData: [ 1, 0 ]
 }, {
 targets: [ 4 ],
 orderData: [ 4, 0 ]
 } ]
 } );
 } );*/

/**
 * If there is any need of showing the multiple table in a single page
 * then use jequery class selector for initializing the data table
 */
/*$(document).ready(function() {
    $('table.display').DataTable();
} );*/


/**
 * The column that is hidden is still part of the table and
 * can be made visible through the api column().visible() API method
 * at a future time if you wish to have columns which can be shown and hidden.
 *
 * Furthermore, as the hidden data is still part of the table,
 * it can still, optionally, be filtered upon allowing the user access to that data
 * (for example 'tag' information for a row entry might used).
 *
 * In the table, both the office and age version
 * columns have been hidden, the former is not searchable, the latter is.
 */
/*$(document).ready(function() {
    $('#example').DataTable( {
        "columnDefs": [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 3 ],
                "visible": false
            }
        ]
    } );
} );*/


/**
 * Vertical Scroll
 */
/*$(document).ready(function() {
    $('#example').DataTable( {
        "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false
    } );
} );*/


/**
 * Horizontal Scroll
 */
/*$(document).ready(function() {
    $('#example').DataTable( {
        "scrollX": true
    } );
} );*/


/**
 * File Export
 */
/*$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );*/

/**
 * File Export [HTML5 Button]
 */
/*
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );*/


/**
 * Column visible Bootstrap
 */
/*$(document).ready(function() {
    var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );

    table.buttons().container()
        .appendTo( '#example_wrapper .small-6.columns:eq(0)' );
} );*/


/**
 * Page length options
 */
/*$(document).ready(function() {
    var table = $('#example').DataTable( {
        "lengthMenu": [[10, 15, 25, 50, -1], ['Ten', 'Fifteen', 'Twenty-Five', 'Fifty', 'All']]
    } );

    table.buttons().container()
        .appendTo( '#example_wrapper .small-6.columns:eq(0)' );
} );*/

/**
 * Column visibility
 */
$(document).ready(function() {
    var t = $('#example').DataTable( {
        "order": [[ 1, 'asc' ]],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print', 'colvis'
        ]
    });

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
} );
